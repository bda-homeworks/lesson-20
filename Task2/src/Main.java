import Annotations.HardwareAnnotation;
import Annotations.SoftwareAnnotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class Main {
    public static void main(String[] args) {
        Tech phone = new Tech("Samsung","OneOS","1440X2560","Android");

        Class<?> classPhone = phone.getClass();
        Field[] fields = classPhone.getDeclaredFields();

        for (Field field : fields) {
            Annotation annotation = field.getAnnotation(HardwareAnnotation.class);
            if (annotation != null) {
                String fieldName = field.getName();
                HardwareAnnotation timeManageAnno = (HardwareAnnotation) annotation;
                String value = timeManageAnno.value();
                System.out.println(fieldName + ": " + value);
            }
        }
        for (Field field : fields) {
            Annotation annotation = field.getAnnotation(SoftwareAnnotation.class);
            if (annotation != null) {
                String fieldName = field.getName();
                SoftwareAnnotation timeManageAnno = (SoftwareAnnotation) annotation;
                String value = timeManageAnno.value();
                System.out.println(fieldName + ": " + value);
            }
        }
    }
}
