import Annotations.HardwareAnnotation;
import Annotations.SoftwareAnnotation;

public class Tech {
    @HardwareAnnotation("Samsung")
    String phones;
    @SoftwareAnnotation("OneOS")
    String operationSystem;
    @HardwareAnnotation("1440X2560")
    String displayResolution;
    @SoftwareAnnotation("Android")
    String kernel;

    public String getPhones() {
        return phones;
    }

    public String getOperationSystem() {
        return operationSystem;
    }

    public String getDisplayResolution() {
        return displayResolution;
    }

    public String getKernel() {
        return kernel;
    }

    public Tech(String phones, String operationSystem, String displayResolution, String kernel) {
        this.phones = phones;
        this.operationSystem = operationSystem;
        this.displayResolution = displayResolution;
        this.kernel = kernel;
    }
}
