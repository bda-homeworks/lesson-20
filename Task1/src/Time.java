import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class Time {
    @TimeManage("23")
    int hour;
    @TimeManage("11")
    int minute;
    @TimeManage("48")
    int second;
    @TimeManage("975")
    int nanosecond;

    public static void main(String[] args) {
        Time time = new Time();
        Class<?> timeClass = time.getClass();
        Field[] fields = timeClass.getDeclaredFields();

        for (Field field : fields) {
            Annotation annotation = field.getAnnotation(TimeManage.class);
            if (annotation != null) {
                String fieldName = field.getName();
                TimeManage timeManageAnno = (TimeManage) annotation;
                String value = timeManageAnno.value();
                System.out.println(fieldName + ": " + value);
            }
        }
    }
}
